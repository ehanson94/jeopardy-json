const fetch = require('node-fetch')
const fs = require('fs')

const getRandomNumber = (length) => Math.floor(Math.random() * length)

const categoryCount = 6

async function getCategories () {
  const response = await fetch('https://jservice.kenzie.academy/api/categories')
  const { categories } = await response.json()
  const selectedCategories = []

  for (let iter = 0; iter < categoryCount; iter++) {
    const index = getRandomNumber(categories.length)
    selectedCategories.push(categories[index])
  }

  fs.writeFileSync('categories.json', JSON.stringify(selectedCategories))
  console.log('Categories.json saved!')
}

getCategories()

// axios.get('https://jservice.kenzie.academy/api/categories')
//   .then(response => {
//     fs.writeFileSync('categories.json', JSON.stringify(response.data))
//   })

//we want persistent state. we're going to use files.

console.log('Does this happen before or after the console.log inside our fs.writeFile callback??')